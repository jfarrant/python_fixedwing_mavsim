"""
TO DO:
One more numerical improvement. Euler integration is not very accurate for quaternions. I often use exact discretization (zero-order hold). It is possible to write the attitude dynamics as 

   d/dt q = Tw * q           

where Tw is a matrix that depends on the angular rates w = [p q r ]’. If you download the updated Tquat.m from the MSS toolbox you can see how I compute Tq and Tw. You have so far only used Tq in your code so you need to add a function for Tw. After computing Tw, you can do exact discretization according to

   q = expm( Tw * h) * q       // this replaces Euler integration for q and the normalization step below

You do not need to normalize after this step since q and expm both are unity length. Note that expm is the Matlab matrix exponential, which you can find in numpy as well. Also note that this is the method you use for linear systems, d/dt x = A x, which yields

   x (t) = expm ( A * (t-t0)  ) x(0)

"""


"""
msgState 
    - messages type for state, that will be passed between blocks in the architecture
    
part of mavPySim 
    - James Farrant 11/11/2021
    - Update history:  

Length of Assembly = 39.5 inches, 1.0033 meters
Outer Diamter = 3inches, 0.0762 meters
Dry weight = 16.69 lb, 74.3 newtons #Verified
mass 0.519 slug, 7.5742357 kg #Verified
buoyant force = 9.16 lb, 40.746 Newtons (about 7.6 lb negatively buoyant)

Inertia matrix
in slug-ft2
[[3.83e-03, -8.34e-07, 3.77e-05]
 [-8.348e-07, 0.388, -5.64e-04 ]
 [3.77e-05, -5.64e-04, 0.388   ]]

in kgm2
[[0.0051927828, -0.0000011308, 0.0000511143 ]
 [-0.0000011318, 0.5260573692, -0.0007646813]
 [0.0000511143, -0.0007646813, 0.5260573692 ]]

original: -18.75 from nose
changed CO to the midpoint of the vehicle:
[-17.95, 0, 0] inches, [-0.47625, 0, 0] meters

CG w.r.t CO
[0.62-0.8, 0, -0.01] inches, [-0.004572, 0, 0.000254 ] meters
-0.18
CB w.r.t CO
[0.46-0.8, 0, 0] inches, [-0.008636, 0, 0] meters
-0.34

spheroid appx of mass= 25.04 kg
4/3.0 * pi * rho * length * diameter^2

----------------------------------------------------------------
                       Quaternions
----------------------------------------------------------------

"""

import numpy as np
from math import cos, sin
np.set_printoptions(precision=4, suppress=True)

#1026.0
class msgState:
    def __init__(self, mu = 47.8209, rho = 1026.0, L_auv=1.0033, D_auv=0.0762):
        """
        self.u = 0.0
        self.v = 0.0
        self.w = 0.0
        self.p = 0.0
        self.q = 0.0
        self.r = 0.0
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0
        self.eta = 0.0
        self.eps1 = 0.0
        self.eps2 = 0.0
        self.eps3 = 0.0
        """

        self.x = np.zeros((13, 1))
        self.q = euler2q(self.x.item(10), self.x.item(11), self.x.item(12))

        #Update states with quaternions 
        self.x[9] = self.q[0]
        self.x[10] = self.q[1]
        self.x[11] = self.q[2]
        self.x[12] = self.q[3]
        
        self.phiDisplay = 0.0
        self.thetaDisplay = 0.0
        self.psiDisplay = 0.0

        """ Constants """
        self.g_mu = 9.7803253359 * (1 + 0.001931850400 * np.sin(mu)**2) / np.sqrt(1 - 0.006694384442 * np.sin(mu)**2)
        self.rho = rho

        """ State vectors and control inputs """
        self.nu = self.x[0:6] # 0:5
        #self.eta = self.x[6:12] # 6:11
        self.eta = self.x[6:13] # 6:12

        self.U = 0.0                            # Initial Speed
        self.U_r = 0.0                          # Initial relative speed

        self.nu_r = self.nu
        """ AUV model parameters """
        self.L_auv = L_auv                      # AUV length (m)
        self.D_auv = D_auv                       # AUV diamater (m)
        self.CD_0 = 0.2                         # parasitic drag
        self.S = 0.7 * self.L_auv * self.D_auv  # S = 70% of rectangle L_auv * D_auv
        self.a = L_auv/2                        # semi-axes
        self.b = D_auv/2
        
        self.r_bg = np.array([[0.004572],
                              [0],
                              [0.02]])          # CG w.r.t. to the CO

        self.r_bb = np.array([[-0.008636],
                              [0],
                              [0]])             # CB w.r.t. to the CO
        
        #-0.004572
##        self.r_bg = np.array([[0.2],
##                              [0],
##                              [0.254]])          # CG w.r.t. to the CO
##        #-0.008636
##        self.r_bb = np.array([[-0.008636],
##                              [0],
##                              [0]])             # CB w.r.t. to the CO
                   
        """ Added moment of inertia in roll """
        self.r44 = 0.3               # A44 = r44 * Ix

        """ Propeller data """
        self.n = 0.0                 # Initial Propulsion RPM
        self.D_prop = 0.055          # propeller diameter
        self.KT = 0.4739             # [KT, KQ] = wageningen(0,1,0.8,3)
        self.KQ = 0.0730

        """ Tail rudder """
        self.delta_r = 0.0           # Initial tail rudder position
        self.CL_delta_r = 0.5        # rudder lift coefficient
        self.A_r = 0.10 * 0.05       # rudder area (m2)
        self.x_r = -self.a           # rudder x-position (m)

        """ Stern plane """
        self.delta_s = 0.0           #Initial Stern plane position
        self.CL_delta_s = 0.7        # stern-plane lift coefficient
        self.A_s = 2 * 0.10 * 0.05   # stern-plane area (m2)
        self.x_s = -self.a           # stern-plane z-position (m)
        self.alpha = 0.0             # Initial angle of attack

        """ Low-speed linear damping matrix parameters: D * exp(-3 * U_r) """
        self.T1 = 20                 # time constant in surge (s)
        self.T2 = 20                 # time constant in sway (s)
        self.zeta4 = 0.3             # relative damping ratio in roll
        self.zeta5 = 0.8             # relative damping ratio in pitch
        self.T6 = 5                  # time constant in yaw (s)

        """ mass and added mass """
        self.MRB,self.CRB = spheroid(self.a,self.b,self.nu[3:6],self.r_bg, self.rho)
        self.MA,self.CA = imlay61(self.a, self.b, self.nu_r, self.r44, self.rho)

        self.M = self.MRB + self.MA
        self.C = self.CRB + self.CA

        self.m = self.MRB[0][0]
        self.W = self.m * self.g_mu
        self.B = 40.746 #self.W -10          # self.B == self.W Assumes we have a nuetrally buoyant AUV

    def controlInput(self, ui, Vc=0.0, betaVc=0.0):
        
        if(len(ui) != 3):
            raise Exception("u-vector must have dimension 3!")
        
        """ Update State vectors """
        #self.nu = np.array([[self.u, self.v, self.w, self.p, self.q, self.r]]).T
        #self.eta = np.array([[self.x, self.y, self.z, self.phi, self.theta, self.psi]]).T
        
        
        self.nu = self.x[0:6] #v (u, v, w, p, q, r)
        #self.eta = self.x[6:12] #n
        self.eta = self.x[6:13] # 6:12
        #self.q = euler2q(self.x.item(9), self.x.item(10), self.x.item(11))

        self.delta_r = np.array(ui[0]) # Tail Rudder (Rad)
        self.delta_s = np.array(ui[1]) # Stern Plane (Rad)
        self.n = np.array(ui[2]/60)    # propeller revolution (rps)

        """ Ocean currents expressed in Body """
        u_c = Vc * np.cos(betaVc - self.eta[5])
        v_c = Vc * np.sin(betaVc - self.eta[5])
        
        """ Amplitude Saturation of control signals """
        max_ui = np.array([30*np.pi/180, 50*np.pi/180, 11000/60]) # deg, deg, rps

        """ Relative velocities, speed and angle of attack """
        self.nu_r = self.nu - np.array([[float(u_c), float(v_c), 0.0, 0.0, 0.0, 0.0]]).T                   # relative velocity
        self.alpha = np.arctan2(self.nu_r[2], self.nu_r[0])                # angle of attack (rad)
        self.U_r = np.sqrt( self.nu_r[0]**2 + self.nu_r[1]**2 + self.nu_r[2]**2 )  # relative speed (m/s)
        self.U  = np.sqrt( self.nu[0]**2 + self.nu[1]**2 + self.nu[2]**2 )         # speed (m/s)

        """ nonlinear quadratic velocity terms in pitch and yaw (Munk moments) 
            are cancelled since only linear damping is used
        """
        self.MRB,self.CRB = spheroid(self.a,self.b,self.nu[3:6],self.r_bg)
        self.MA,self.CA = imlay61(self.a, self.b, self.nu_r, self.r44)
        
        self.CA[4][0] = 0   
        self.CA[4][3] = 0
        self.CA[5][0] = 0
        self.CA[5][1] = 0
        
        self.M = self.MRB + self.MA
        self.C = self.CRB + self.CA
        
        self.m = self.MRB[0][0]
        #self.W = self.m * self.g_mu
        #self.B = self.W
        
        """ dissipative forces and moments """
        D = Dmtrx([self.T1, self.T2, self.T6],[self.zeta4, self.zeta5], self.M, [self.W, self.r_bg.T, self.r_bb.T])
        D[0][0] = D[0][0] * np.exp(-3 * self.U_r)   # vanish at high speed where crossflow
        D[1][1] = D[1][1] * np.exp(-3 * self.U_r)   # drag and lift/drag dominates
        D[5][5] = D[5][5] * np.exp(-3 * self.U_r)
        
        tau_liftdrag = forceLiftDrag(self.D_auv,self.S,self.CD_0,self.alpha,self.U_r, self.rho)
        tau_crossflow = crossFlowDrag(self.L_auv,self.D_auv,self.D_auv,self.nu_r, self.rho)

        """ restoring forces and moments """
        #g = gvect(self.W,self.B,self.eta[4],self.eta[3],self.r_bg,self.r_bb)
        #print("g Matrix:\n", g)
        g = gvect(self.W,self.B,self.q,self.r_bg,self.r_bb)

        """ kinematics """
        #J = eulerang(self.eta[3],self.eta[4],self.eta[5])
        J = quatern(self.q)
        """ amplitude saturation of the control signals """
        
        if (abs(self.delta_r) > max_ui[0]):
            self.delta_r = np.sign(self.delta_r) * max_ui[0]
            
        if (abs(self.delta_s) > max_ui[1]):
            self.delta_s = np.sign(self.delta_s) * max_ui[1]
            
        if (abs(self.n) > max_ui[2]):
            self.n = np.sign(self.n) * max_ui[2]
            
        """ control forces and moments """
        
        X_prop = self.rho * self.D_prop**4 * self.KT * abs(self.n) * self.n  # propeller thrust 
        K_prop = self.rho * self.D_prop**5 * self.KQ * abs(self.n) * self.n  # propeller-induced roll moment
        X_r = -0.5 * self.rho * self.U_r**2 * self.A_r * self.CL_delta_r * self.delta_r**2  # rudder drag
        Y_r = -0.5 * self.rho * self.U_r**2 * self.A_r * self.CL_delta_r * self.delta_r    # rudder sway force
        N_r = self.x_r * Y_r                                          # rudder yaw moment
        X_s = -0.5 * self.rho * self.U_r**2 * self.A_s * self.CL_delta_s * self.delta_s**2  # stern-plane drag
        Z_s = -0.5 * self.rho * self.U_r**2 * self.A_s * self.CL_delta_s * self.delta_s    # stern-plane heave force
        M_s =  self.x_s * Z_s                                         # stern-plane pitch moment
        tau = np.zeros((6, 1))                                # generalized force vector
        tau[0] = X_prop + X_r + X_s
        tau[1] = Y_r
        tau[2] = Z_s
        tau[3] = K_prop
        tau[4] = M_s 
        tau[5] = N_r
        #print("Y_r: ", Y_r)
        #print("Z_S: ", Z_s)
        """ state-space model """
        #print("tau: \n", tau)
        #print("tau_liftdrag: \n", tau_liftdrag)
        #print("tau_crossflow: \n", tau_crossflow)
        #print("C: \n", self.C)
        #print("self.nu_r: \n", self.nu_r)
        #print("D: \n", D)
        #print("g \n", g)
        #print(np.linalg.inv(self.M))
        xdot1 = np.linalg.inv(self.M) @ (tau + tau_liftdrag + tau_crossflow - self.C @ self.nu_r - D @ self.nu_r - g)
        xdot2 = J @ self.nu #nu[0:2] = u, v, w; nu[3:5] = p, q, r
        #print("xdot1: \n", xdot1)
        #print("xdot2: \n", xdot2)
        xdot = np.concatenate((xdot1, xdot2), axis=0)
        #print("xdot: \n", xdot)
        return xdot, self.U
    
    def euler2(self, xdot, h):
        self.x += xdot * h

        #Normalize q, and update quaternion state
        q = self.x[9:13]
        
        self.q = q/np.linalg.norm(q)
        self.x[9] = self.q[0]
        self.x[10] = self.q[1]
        self.x[11] = self.q[2]
        self.x[12] = self.q[3]
        #print("quats normalized:", self.q)
        

        self.phiDisplay, self.thetaDisplay, self.psiDisplay = q2euler(self.q)
        
        


def spheroid(a, b, nu2, r_bg, rho=1026.0):
    O3 = np.zeros((3, 3))
    #This is the spheroid estimation function... currently replaced with Leidos estimated mass properties
    #m = 4/3.0 * np.pi * rho * a * b**2

    #moment of inertia spheroid estimates
    #Ix = (2/5.0) * m * b**2
    #Iy = (1/5.0) * m * (a**2 + b**2)
    #Iz = Iy
    #Ig = np.array([[Ix, 0.0, 0.0], [0.0, Iy, 0.0], [0.0, 0.0, Iz]])
    
    #Leidos Estimate
    m = 7.5742357 #kg
    
    #Leidos Esimates
    Ix = 0.0051927828
    Iy = 0.5260573692
    Iz = 0.5260573692
    #[[0.0051927828, -0.0000011308, 0.0000511143 ]
    # [-0.0000011318, 0.5260573692, -0.0007646813]
    # [0.0000511143, -0.0007646813, 0.5260573692 ]]
    
    Ig = np.array([[Ix, -0.0000011308, 0.0000511143], [-0.0000011318, Iy, -0.0007646813], [0.0000511143, -0.0007646813, Iz]])

    #rigid-body matrices expressed in the CG
    MRB_CG = np.array([[m, 0, 0, 0,  0, 0 ],
                       [0, m, 0, 0,  0, 0 ],
                       [0, 0, m, 0,  0, 0 ],
                       [0, 0, 0, Ix, 0, 0 ],
                       [0, 0, 0, 0,  Iy, 0],
                       [0, 0, 0, 0,  0, Iz]])


    CRB_CG_00 = m * Smtrx(nu2)
    xxx = np.concatenate((CRB_CG_00, O3), axis=1)
    yyy = np.concatenate((O3, -Smtrx(Ig@nu2)), axis=1)

    CRB_CG = np.concatenate((xxx, yyy), axis=0)

    H = Hmtrx(r_bg)
    MRB = H.T @ MRB_CG @ H
    CRB = H.T @ CRB_CG @ H

    return MRB, CRB

def Smtrx(a):
    a = a.flatten()
    S = np.array([[0, -a[2], a[1]],
                  [a[2], 0, -a[0]],
                  [-a[1], a[0], 0]])
    return S

def Hmtrx(r):
    S = Smtrx(r)
    H1 = np.concatenate((np.eye(3), S.T), axis=1)
    H2 = np.concatenate((np.zeros((3, 3)), np.eye(3)), axis=1)
    H = np.concatenate((H1, H2), axis=0)
    return H
    
def imlay61(a, b, nu, r44=0.0, rho=1026.0):

    #m = 4/3.0 *np.pi * rho * a * b**2
    #Ix = (2/5.0) * m * b**2
    #Iy = (1/5.0) * m * (a**2 + b**2)

    #Leidos Estimate
    m = 7.5742357 #kg
    #Leidos Esimates
    Ix = 0.0051927828
    Iy = 0.5260573692
    


    if(a < 0):
        raise ValueError("a must be larger than 0!")

    if(b < 0):
        raise ValueError("b must be larger than 0!")

    if(a <= b):
        raise ValueError("a must be larger than b!")

    MA_44 = r44 * Ix
    e = 1 - (b/a)**2
    alpha_0 = ( 2 * (1-e**2)/e**3 ) * ( 0.5 * np.log((1+e)/(1-e)) - e )  
    beta_0  = 1/e**2 - (1-e**2)/(2*e**3) * np.log((1+e)/(1-e)) 
    k1 = alpha_0 / (2 - alpha_0)
    k2 = beta_0  / (2 - beta_0)
    k_prime = e**4*(beta_0-alpha_0) / ((2-e**2)*(2*e**2-(2-e**2)*(beta_0-alpha_0)))

    MA = np.array([[m*k1, 0, 0, 0,  0, 0 ],
                   [0, m*k2, 0, 0,  0, 0 ],
                   [0, 0, m*k2, 0,  0, 0 ],
                   [0, 0, 0, MA_44, 0, 0 ],
                   [0, 0, 0, 0,  k_prime*Iy, 0],
                   [0, 0, 0, 0,  0, k_prime*Iy]])

    CA = m2c(MA,nu)

    return MA, CA

def m2c(M, nu):
    
    #Makes M a symmetric matrix
    M = 0.5 * (M + M.T)

    if(len(nu) == 6):
        M11 = M[0:3, 0:3]
        M12 = M[0:3, 3:6]
        M21 = M12.T
        M22 = M[3:6, 3:6]

        nu1 = nu[0:3]
        nu2 = nu[3:6]

        dt_dnu1 = M11 @ nu1 + M12 @ nu2
        dt_dnu2 = M21 @ nu1 + M22 @ nu2

        CC1 = np.concatenate((np.zeros((3, 3)), -Smtrx(dt_dnu1)), axis=1)
        CC2 = np.concatenate((-Smtrx(dt_dnu1), -Smtrx(dt_dnu2)), axis=1)
        C = np.concatenate((CC1, CC2), axis=0)
    
    else:
        C = np.array([[0, 0, -M[1][1]*nu[1]-M[1][2]*nu[2]],
                      [0, 0, M[0][0]*nu[0],
                       M[1][1]*nu[1]+M[1][2]*nu[2], -M[0][0]*nu[0], 0]])
    
    return C

def Dmtrx(T, zeta, M, hydrostatics):
    #  AUV:    D = Dmtrx([5 10 10], [0.2, 0.3], MRB, MA,[m*g, [0,0,zg], [0,0,0])
    T1 = T[0]
    T2 = T[1]
    T6 = T[2]
    zeta4 = zeta[0]
    zeta5 = zeta[1]
        
    W = hydrostatics[0]

    r_bg = [hydrostatics[1].item(0), hydrostatics[1].item(1), hydrostatics[1].item(2)]
    r_bb = [hydrostatics[2].item(0), hydrostatics[2].item(1), hydrostatics[2].item(2)]

    
    T3 = T2       # for AUVs, assume same time constant in sway and heave
    w4 = np.sqrt( W * (r_bg[2]-r_bb[2]) / M[3][3] )
    w5 = np.sqrt( W * (r_bg[2]-r_bb[2]) / M[4][4] )
    D = np.array([[M[0][0]/T1, 0, 0, 0, 0, 0],
                  [0, M[1][1]/T2, 0, 0, 0, 0],
                  [0, 0, M[2][2]/T3, 0, 0, 0],
                  [0, 0, 0, M[3][3]*2*zeta4*w4, 0, 0],
                  [0, 0, 0, 0, M[4][4]*2*zeta5*w5, 0],
                  [0, 0, 0, 0, 0, M[5][5]/T6]])
    
    return D

def forceLiftDrag(b,S,CD_0,alpha,U_r,rho):
    """
    % Output:
    %  tau_liftdrag:  6x1 generalized force vector
    %
    % Inputs:
    %  b:       wing span (m)
    %  S:       wing area (m^2)
    %  CD_0:    parasitic drag (alpha = 0), typically 0.1-0.2 for a streamlined body
    %  alpha:   angle of attack, scalar or vector (rad)
    %  U_r:     relative speed (m/s)
    %
    % Example:
    %
    % Cylinder-shaped AUV with length L = 1.8, diameter D = 0.2 and CD_0 = 0.1:
    %    tau_liftdrag = forceLiftDrag(0.2, 1.8*0.2, 0.1, alpha, U_r)
    """
    
    CL,CD = coeffLiftDrag(b,S,CD_0,alpha, 0)
    F_drag = 0.5 * rho * U_r[0]**2 * S * CD[0]    # drag force
    F_lift = 0.5 * rho * U_r[0]**2 * S * CL[0]    # lift force
    # transform from FLOW axes to BODY axes using angle of attack
    tau_liftdrag = np.array([[np.cos(alpha[0]) * (-F_drag) - np.sin(alpha[0]) * (-F_lift),
                              0,
                              np.sin(alpha[0]) * (-F_drag) + np.cos(alpha[0]) * (-F_lift),
                              0,
                              0,
                              0]]).T
    return tau_liftdrag
    
def crossFlowDrag(L,B,T,nu_r, rho):
    """
    % Inputs: L:  length
    %         B:  beam
    %         T:  draft 
    %         nu_r = [u-u_c, v-v_c, w-w_c, p, q, r]': relative velocity vector
    %
    % Output: tau_crossflow = [0 Yh 0 0 0 Nh]:  cross-flow drag in sway and yaw
    %
    """  
    n = 20                 # number of strips
    dx = L/20             
    Cd_2D = Hoerner(B,T)   # 2D drag coefficient based on Hoerner's curve
    Yh = 0
    Nh = 0
    xL = np.arange(-L/2, L/2, dx).tolist()
    for i in range(len(xL)):
        #print(i)
        v_r = nu_r.item(1)          # relative sway velocity
        r = nu_r.item(5)            # yaw rate
        Ucf = abs(v_r + xL[i] * r) * (v_r + xL[i] * r)
        Yh = Yh - 0.5 * rho * T * Cd_2D * Ucf * dx         # sway force
        Nh = Nh - 0.5 * rho * T * Cd_2D * xL[i] * Ucf * dx    # yaw moment

    tau_crossflow = np.array([[0, Yh, 0, 0, 0, Nh]]).T
    #print("tau_crossflow: \n", tau_crossflow)
    return tau_crossflow

#https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.56.2460&rep=rep1&type=pdf
##def gvect(W,B,theta,phi,r_bg,r_bb):
##    sth  = np.sin(theta[0])
##    cth  = np.cos(theta[0])
##    sphi = np.sin(phi[0])
##    cphi = np.cos(phi[0])
##    #print("W: ", W)
##    #print("B: ", B)
##
##    g = np.array([[(W-B) * sth,
##                   -(W-B) * cth * sphi,
##                   -(W-B) * cth * cphi,
##                   -(r_bg.item(1)*W-r_bb.item(1)*B) * cth * cphi + (r_bg.item(2)*W-r_bb.item(2)*B) * cth * sphi,
##                   (r_bg.item(2)*W-r_bb.item(2)*B) * sth + (r_bg.item(0)*W-r_bb.item(0)*B) * cth * cphi,
##                   -(r_bg.item(0)*W-r_bb.item(0)*B) *cth * sphi  - (r_bg.item(1)*W-r_bb.item(1)*B) * sth]]).T
##    
##    return g

def gvect(W,B,q,r_bg,r_bb):
    #sth  = np.sin(theta[0])
    #cth  = np.cos(theta[0])
    #sphi = np.sin(phi[0])
    #cphi = np.cos(phi[0])
    #print("W: ", W)
    #print("B: ", B)
    R = Rquat(q)
    

    g = np.array([[-(W-B) * R.item((2, 0)),
                   -(W-B) * R.item((2, 1)),
                   -(W-B) * R.item((2, 2)),
                   -(r_bg.item(1)*W - r_bb.item(1)*B) * R.item((2, 2)) + (r_bg.item(2)*W - r_bb.item(2)*B) * R.item((2, 1)),
                   -(r_bg.item(2)*W - r_bb.item(2)*B) * R.item((2, 0)) + (r_bg.item(0)*W - r_bb.item(0)*B) * R.item((2, 2)),
                   -(r_bg.item(0)*W - r_bb.item(0)*B) *R.item((2, 1))  + (r_bg.item(1)*W - r_bb.item(1)*B) * R.item((2, 0))]]).T
    
    return g

def eulerang(phi,theta,psi):

    J1 = Rzyx(phi,theta,psi)
    J2 = Tzyx(phi,theta)

    H1 = np.concatenate((J1, np.zeros((3, 3))), axis=1)
    H2 = np.concatenate((np.zeros((3, 3)), J2), axis=1)
    J = np.concatenate((H1, H2), axis=0)
 
    return J

def Rzyx(phi, theta, psi):
    cphi = np.cos(phi[0])
    sphi = np.sin(phi[0])
    cth  = np.cos(theta[0])
    sth  = np.sin(theta[0])
    cpsi = np.cos(psi[0])
    spsi = np.sin(psi[0])

    R = np.array([[cpsi*cth, -spsi*cphi+cpsi*sth*sphi, spsi*sphi+cpsi*cphi*sth],
                  [spsi*cth, cpsi*cphi+sphi*sth*spsi, -cpsi*sphi+sth*spsi*cphi],
                  [-sth, cth*sphi, cth*cphi]])
    #print("R: \n", R)
    return R

def Tzyx(phi, theta):
    cphi = np.cos(phi[0])
    sphi = np.sin(phi[0])
    cth  = np.cos(theta[0])
    sth  = np.sin(theta[0])

    if cth==0:
        raise ValueError('Tzyx is singular for theta = +-90 degrees')
    T = np.array([[1,  sphi*sth/cth,  cphi*sth/cth],
                  [0,  cphi,          -sphi],
                  [0,  sphi/cth,      cphi/cth]])
    #print("T: \n", T)
    return T


    

def Hoerner(B,T):

    x =[0.0108623,0.176606,0.353025,0.451863,0.472838,0.492877,0.493252,0.558473,0.646401,0.833589,0.988002,1.30807,1.63918,1.85998,2.31288,2.59998,3.00877,3.45075,3.7379,4.00309]
    y = [1.96608,1.96573,1.89756,1.78718,1.58374,1.27862,1.21082,1.08356,0.998631,0.87959,0.828415,0.759941,0.691442,0.657076,0.630693,0.596186,0.586846,0.585909,0.559877,0.559315]

    return  np.interp(B/(2*T), x, y)

def coeffLiftDrag(b,S,CD_0,alpha,sigma):
    """
    % Outputs:
    %  CL:      lift coefficient as a function of alpha   
    %  CD:      drag coefficient as a function of alpha   
    %
    % Inputs:
    %  b:       wing span (m)
    %  S:       wing area (m^2)
    %  CD_0:    parasitic drag (alpha = 0), typically 0.1-0.2 for a streamlined body
    %  alpha:   angle of attack, scalar or vector (rad)
    %  sigma:   blending parameter between 0 and 1, use sigma = 0 for linear lift 
    %  display: use 1 to plot CD and CL (optionally)

    """
    e = 0.7             # Oswald efficiency number
    AR = b**2/S         # wing aspect ratio
    # linear lift
    CL_alpha = np.pi * AR / ( 1 + np.sqrt(1 + (AR/2)**2) )
    CL = CL_alpha * alpha  
    # parasitic and induced drag
    CD = CD_0 + np.square(CL) / (np.pi * e * AR) 
    # nonlinear lift (blending function)
    CL = (1-sigma) * CL + sigma * 2 * np.sign(alpha) * np.square(np.sin(alpha))*np.cos(alpha)

    return CL, CD

"""
function R = Rquat(q)
% R = Rquat(q) computes the quaternion rotation matrix R in SO(3)
% for q = [eta eps1 eps2 eps3]
%
% Author:    Thor I. Fossen
% Date:      14th June 2001
% Revisions: 6 October 2001, T I. Fossen - eta as first element in q  

tol = 1e-6;
if abs(norm(q)-1)>tol; error('norm(q) must be equal to 1'); end

eta = q(1);
eps = q(2:4);

S = Smtrx(eps);
R = eye(3) + 2*eta*S + 2*S^2;
"""
def Rquat(q):
    tol = 1e-6
    if((np.linalg.norm(q) - 1) > tol):
        raise ValueError("normalization of q must be equal to 1")

    eta = q.item(0)
    eps = q[1:3 +1]

    S = Smtrx(eps)
    #print("S: \n", S)
    R = np.eye(3) + 2*eta*S + 2*S@S
    #print("eta: \n", eta)
    #print("eps: \n", eps)
    #print("Rquat: \n", R)
    return R

"""
function Tq = Tquat(q)
% Tq = TQUAT(q) computes the quaternion transformation matrix for attitude
%
% Author:   Thor I. Fossen
% Date:     10th September 2010
% Revisions: 

eta  = q(1); eps1 = q(2); eps2 = q(3); eps3 = q(4); 
 
Tq = 0.5*[...
   -eps1 -eps2 -eps3        
    eta  -eps3  eps2
    eps3  eta  -eps1
   -eps2  eps1  eta   ];
"""
def Tquat(q):
    eta = q.item(0)
    eps1 = q.item(1)
    eps2 = q.item(2)
    eps3 = q.item(3)

    Tq = 0.5*np.array([[-eps1, -eps2, -eps3],
                       [eta, -eps3, eps2],
                       [eps3, eta, -eps1],
                       [-eps2, eps1, eta]])

    return Tq

"""
function q = euler2q(phi,theta,psi)
% q = EULER2Q(phi,theta,psi) computes the unit quaternions q = [eta eps1 eps2 eps3]
% from Euler angles phi, theta and psi
%
% Author:    Thor I. Fossen
% Date:      8th June 2000
% Revisions: 6 October 2001, T I. Fossen - eta as first element in q 
%            16 April 2019, T. I. Fossen - replaced Sheppard with NASA algorithm
%
% NASA Mission Planning and Analysis Division. "Euler Angles, Quaternions, 
% and Transformation Matrices" (PDF). NASA. Retrieved 12 January 2013.

cy = cos(psi * 0.5);
sy = sin(psi * 0.5);
cp = cos(theta * 0.5);
sp = sin(theta * 0.5);
cr = cos(phi * 0.5);
sr = sin(phi * 0.5);

q = [cy * cp * cr + sy * sp * sr
     cy * cp * sr - sy * sp * cr
     sy * cp * sr + cy * sp * cr
     sy * cp * cr - cy * sp * sr];

q = q/(q'*q);
"""
def euler2q(phi, theta, psi):
    cy = cos(psi * 0.5)
    sy = sin(psi * 0.5)
    cp = cos(theta * 0.5)
    sp = sin(theta * 0.5)
    cr = cos(phi * 0.5)
    sr = sin(phi * 0.5)

    q = np.array([[cy * cp * cr + sy * sp * sr,
                   cy * cp * sr - sy * sp * cr,
                   sy * cp * sr + cy * sp * cr,
                   sy * cp * cr - cy * sp * sr]]).T

    q = q / (q.T @ q)
    #print("euler 2 q init: ", q)

    return q


"""
function [phi,theta,psi] = q2euler(q)
% [phi,theta,psi] = Q2EULER(q) computes the Euler angles from the unit 
% quaternions q = [eta eps1 eps2 eps3]
%
% Author:   Thor I. Fossen
% Date:      2001-06-14  
% Revisions: 2007-09-03  Test for singular solution theta = +-90 deg has been improved

R = Rquat(q);
if abs(R(3,1))>1.0, error('solution is singular for theta = +- 90 degrees'); end
 
phi   = atan2(R(3,2),R(3,3));
theta = -asin(R(3,1));
psi   = atan2(R(2,1),R(1,1));
"""
def q2euler(q):

    q = q / np.linalg.norm(q) # Handle roundoff errors.
    R = Rquat(q)
    #print("q: \n", q)
    #print("R: \n", R)
    #print("R(2,0): ", R.item((2, 0)))

    phi   = np.arctan2(R.item((2, 1)), R.item((2, 2)))
##    if(abs(R.item((2, 0))) > 1.0):
##        theta = np.arcsin(1.0 - R.item((2, 0))) - np.pi/2
##    else:
##        theta = -np.arcsin(R.item((2, 0)))
    if (R.item((2, 0)) > 1):
        R[2, 0]  = 1
        #theta = -np.arcsin(R.item((2, 0))) 
    elif (R.item((2, 0)) < -1):
        R[2, 0]  = -1
        #theta = -np.arcsin(R.item((2, 0))) 
    else:
        
        theta = -np.arcsin(R.item((2, 0))) 

    psi   = np.arctan2(R.item((1, 0)), R.item((0, 0)))

    return [phi, theta, psi]

"""
function [J,J1,J2] = quatern(q)
% [J,J1,J2] = QUATERN(q) computes the quaternion transformation matrices
%
% Author:   Thor I. Fossen
% Date:     14th June 2001
% Revisions: 6 October 2001, T I. Fossen - eta as first element in q 

eta  = q(1); eps1 = q(2); eps2 = q(3); eps3 = q(4); 
 
J1 = Rquat(q);
 
J2 = 0.5*[...
   -eps1 -eps2 -eps3        
    eta  -eps3  eps2
    eps3  eta  -eps1
   -eps2  eps1  eta   ];
 
J = [ J1  zeros(3,3);
      zeros(4,3) J2 ];
"""
def quatern(q):
    J11 = Rquat(q)
    J22 = Tquat(q)
    J12 = np.zeros((3, 3))
    J21 = np.zeros((4, 3))

    J1 = np.concatenate((J11, J12), axis=1)
    J2 = np.concatenate((J21, J22), axis=1)
    J = np.concatenate((J1, J2), axis=0)

    return J
    
    
### Test Simulation ###
##dt = 0.1
##MTU = msgState()
##dx, speed = MTU.controlInput([0.5, 0.5, 1000.0])
##MTU.euler2(dx, dt)
##print("MTU States: \n", MTU.x)
##
##dx, speed = MTU.controlInput([0.5, 0.5, 1000.0])
##MTU.euler2(dx, dt)
##print("MTU States: \n", MTU.x)
##dx, speed = MTU.controlInput([0.5, 0.5, 1000.0])
##MTU.euler2(dx, dt)
##print("MTU States: \n", MTU.x)
