#!/usr/bin/env python
import time
from builtins import input
import numpy as np
from Plotter import Plotter
from plotter_args import *

# The plotting_frequency specifies the number of samples taken before the actual plot is updated.
# Increasing the plotting_frequency argument can speed up the real time plotting. 
plotter = Plotter(plotting_frequency=10)

### Define plot names
Thrust_plot = PlotboxArgs(plots = ['Strain_Gauge'],
                        title ="Bridge Voltage (Volts) Vs Time (s)",
                        labels = {'left': 'Voltage (Volts)', 'bottom':'Time (s)'})

## Simple string definitions
first_row = [Thrust_plot]

# Use a list of lists to specify plotting window structure
plots = [first_row]

# Add plots to the window
plotter.add_plotboxes(plots)

# Define and label vectors for more convenient/natural data input
plotter.define_input_vector('Bridge_Voltage', ['Strain_Gauge'])

# Open file to start logging data
f = open("data_log.txt","w")
f.write("time, Strain_Gauge\r\n")

# Create array to store data
BUFF_LEN = 4096
data = []
time = []

# Sample period 60 Hz
dt = 0.025
log_time = 0.0

# Open serial communication with serial port
ser = serial.Serial(port='COM3',
                    baudrate=115200,
                    timeout=None,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE)

ser.reset_input_buffer()

print("Data logging has started... Press (ctrl-c) to end.")

try:
    
    while(True):
        
        # Check if input buffer has data
        if(ser.in_waiting < BUFF_LEN-1):
            
            # If input buffer is not full, append data
            new_data = (float(ser.read_until(b'\r').decode('utf-8')))
            plotter.add_vector_measurement('Bridge_Voltage', [new_data], log_time)
            ## Update and display the plot
            plotter.update_plots()
            data.append(new_data)
            time.append(log_time)
            new_data = str(new_data)

            #print(new_data)
            f.write("{:.2f}, {}\n".format(log_time, new_data))
            log_time += dt
            
        else:

            raise Exception('Input buffer overflow.')  
            
except KeyboardInterrupt:
    print("Data Logging has finished recording.")
    pass

# Close file and serial connection
f.close()
ser.close()

# Plot results
plt.plot(time, data)
plt.xlabel('Time (S)')
plt.ylabel('Bridge Voltage (Volts)')
plt.title('Bridge Voltage Vs. Time Plot')
plt.grid('minor')
plt.show()
