"""
mavSimPy 
    - Chapter 2 assignment for Beard & McLain, PUP, 2012
    - Update history:  
        2/24/2020 - RWB
"""
import sys
sys.path.append('..')
from chap2.mav_viewer import mavViewer
import parameters.simulation_parameters as SIM
from message_types.msg_state import *
from message_types.msg_path import msgPath
#from message_types.msg_waypoints import msgWaypoints
import time
from builtins import input
import numpy as np
from plotting.Plotter import Plotter
from plotting.plotter_args import *
from math import pi
import matplotlib.pyplot as plt


# initialize messages
state = msgState()  # instantiate state message
# The plotting_frequency specifies the number of samples taken before the actual plot is updated.
# Increasing the plotting_frequency argument can speed up the real time plotting. 
plotter = Plotter(plotting_frequency=10)

### Define plot names
u_plot = PlotboxArgs(plots = ['u'],
                        title ="u Velocity",
                        labels = {'left': 'm/s', 'bottom':'Time (s)'})
v_plot = PlotboxArgs(plots = ['v'],
                        title ="v Velocity",
                        labels = {'left': 'm/s', 'bottom':'Time (s)'})
w_plot = PlotboxArgs(plots = ['w'],
                        title ="w Velocity",
                        labels = {'left': 'm/s', 'bottom':'Time (s)'})
p_plot = PlotboxArgs(plots = ['p'],
                        title ="p Velocity",
                        labels = {'left': 'd/s', 'bottom':'Time (s)'})
q_plot = PlotboxArgs(plots = ['q'],
                        title ="q Velocity",
                        labels = {'left': 'd/s', 'bottom':'Time (s)'})
r_plot = PlotboxArgs(plots = ['r'],
                        title ="r Velocity",
                        labels = {'left': 'd/s', 'bottom':'Time (s)'})
x_plot = PlotboxArgs(plots = ['x'],
                        title ="x Position",
                        labels = {'left': 'm', 'bottom':'Time (s)'})
y_plot = PlotboxArgs(plots = ['y'],
                        title ="y Positions",
                        labels = {'left': 'm', 'bottom':'Time (s)'})
z_plot = PlotboxArgs(plots = ['z'],
                        title ="z position",
                        labels = {'left': 'm', 'bottom':'Time (s)'})
phi_plot = PlotboxArgs(plots = ['phi'],
                        title ="phi Angle",
                        labels = {'left': 'd', 'bottom':'Time (s)'})
theta_plot = PlotboxArgs(plots = ['theta'],
                        title ="theta angle",
                        labels = {'left': 'd', 'bottom':'Time (s)'})
psi_plot = PlotboxArgs(plots = ['psi'],
                        title ="psi angle",
                        labels = {'left': 'm', 'bottom':'Time (s)'})
speed_plot = PlotboxArgs(plots = ['Speed'],
                        title ="RPM Command",
                        labels = {'left': 'RPM', 'bottom':'Time (s)'})
rudder_plot = PlotboxArgs(plots = ['rudder'],
                        title ="rudder angle",
                        labels = {'left': 'd', 'bottom':'Time (s)'})
elevator_plot = PlotboxArgs(plots = ['elevator'],
                        title ="elevator angle",
                        labels = {'left': 'd', 'bottom':'Time (s)'})

## Simple string definitions
first_row = [u_plot, v_plot, w_plot]
second_row = [p_plot, q_plot, r_plot]
third_row = [x_plot, y_plot, z_plot]
fourth_row = [phi_plot, theta_plot, psi_plot]
fifth_row = [speed_plot, rudder_plot, elevator_plot]

# Use a list of lists to specify plotting window structure
plots = [first_row,
         second_row,
         third_row,
         fourth_row,
         fifth_row]

# Add plots to the window
plotter.add_plotboxes(plots)

# Define and label vectors for more convenient/natural data input
plotter.define_input_vector('x velocity', ['u'])
plotter.define_input_vector('y velocity', ['v'])
plotter.define_input_vector('z velocity', ['w'])
plotter.define_input_vector('roll velocity', ['p'])
plotter.define_input_vector('pitch velocity', ['q'])
plotter.define_input_vector('yaw velocity', ['r'])
plotter.define_input_vector('x position', ['x'])
plotter.define_input_vector('y position', ['y'])
plotter.define_input_vector('z position', ['z'])
plotter.define_input_vector('roll angle', ['phi'])
plotter.define_input_vector('pitch angle', ['theta'])
plotter.define_input_vector('yaw angle', ['psi'])
plotter.define_input_vector('RPM Command', ['Speed'])
plotter.define_input_vector('rudder angle', ['rudder'])
plotter.define_input_vector('elevator angle', ['elevator'])
# initialize viewers and video
VIDEO = False  # True==write video, False==don't write video
mav_view = mavViewer()
mav_path = msgPath()
#mav_waypoints = msgWaypoints()

if VIDEO is True:
    from chap2.video_writer import videoWriter
    video = videoWriter(video_name="chap2_video.avi",
                        bounding_box=(0, 0, 1000, 1000),
                        output_rate=SIM.ts_video)

# initialize the simulation time
sim_time = SIM.start_time

#Plot variables
time = []
u =[]
v = []
w = []
p = []
q = []
r = []
x = []
y = []
z = []
phi = []
theta = []
psi = []
RPM_Command = []
rudder_angle = []
elevator_angle = []


# Initilize model in forward flight 
#state.x[10] = -pi/2.5
rudder_input = 0.0
elevator_input = 0.0
thruster_input = 0.0

Heading_Command = np.pi#south
heading_pgain = 0.1
heading_igain = 0.15
heading_dgain = 0.0
errorIntegral =0.0
errorLast = 0.0

depth_Command = 10#south
depth_pgain = 0.1
depth_igain = 0.1
depth_dgain = 0.0
depthErrorIntegral =0.0
depthErrorLast = 0.0

n = 5000
end_sim = SIM.ts_simulation * n
# main simulation loop
while sim_time < end_sim: #SIM.end_time:
    # -------vary states to check viewer-------------
##    if sim_time < SIM.end_time/6:
##        #state.x[6] += 100*SIM.ts_simulation
##        dx, speed = state.controlInput([0.0, 0.0, 0])
##        state.euler2(dx, SIM.ts_simulation)
##    elif sim_time < 2*SIM.end_time/6:
##        dx, speed = state.controlInput([0.0, -0.15, 6000.0])
##        state.euler2(dx, SIM.ts_simulation)
##    elif sim_time < 3*SIM.end_time/6:
##        dx, speed = state.controlInput([0.0, 0.15, 6000.0])
##        state.euler2(dx, SIM.ts_simulation)
##    elif sim_time < 4*SIM.end_time/6:
##        dx, speed = state.controlInput([-0.15, 0.0, 7000.0])
##        state.euler2(dx, SIM.ts_simulation)
##    elif sim_time < 5*SIM.end_time/6:
##        dx, speed = state.controlInput([0.15, 0.0, 7000.0])
##        state.euler2(dx, SIM.ts_simulation)
##    else:
##        dx, speed = state.controlInput([0.0, 0.0, 0.0])
##        state.euler2(dx, SIM.ts_simulation)

    if sim_time > 10:
        #elevator_input = pi/6
        
        heading_error = Heading_Command - state.x.item(11)
        errorIntegral += SIM.ts_simulation * heading_error

        depth_error = depth_Command - state.x.item(8)
        depthErrorIntegral += SIM.ts_simulation * depth_error
        if(abs(depthErrorIntegral) > 30):
            depthErrorIntegral = np.sign(depthErrorIntegral)*30.0
        #print("depthErrorIntegral: ", depthErrorIntegral)
        
        rudder_input = heading_error*(heading_pgain + heading_igain*errorIntegral + heading_dgain*((heading_error-errorLast)/SIM.ts_simulation))
        errorLast = heading_error

        elevator_input = depth_error*(depth_pgain + depth_igain*depthErrorIntegral + depth_dgain*((depth_error-depthErrorLast)/SIM.ts_simulation))
        depthErrorLast = depth_error
        
        if thruster_input < 9000:
            thruster_input += 10

    dx, speed = state.controlInput([rudder_input, elevator_input, thruster_input])
    state.euler2(dx, SIM.ts_simulation)

    plotter.add_vector_measurement('x velocity', [state.x.item(0)], sim_time)
    plotter.add_vector_measurement('y velocity', [state.x.item(1)], sim_time)
    plotter.add_vector_measurement('z velocity', [state.x.item(2)], sim_time)
    plotter.add_vector_measurement('roll velocity', [state.x.item(3)* 180/np.pi], sim_time)
    plotter.add_vector_measurement('pitch velocity', [state.x.item(4)* 180/np.pi], sim_time)
    plotter.add_vector_measurement('yaw velocity', [state.x.item(5)* 180/np.pi], sim_time)
    plotter.add_vector_measurement('x position', [state.x.item(6)], sim_time)
    plotter.add_vector_measurement('y position', [state.x.item(7)], sim_time)
    plotter.add_vector_measurement('z position', [state.x.item(8)], sim_time)
    plotter.add_vector_measurement('roll angle', [state.x.item(9)* 180/np.pi], sim_time)
    plotter.add_vector_measurement('pitch angle', [state.x.item(10)* 180/np.pi], sim_time)
    plotter.add_vector_measurement('yaw angle', [state.x.item(11)* 180/np.pi], sim_time)
    plotter.add_vector_measurement('RPM Command', [state.n*60], sim_time)
    plotter.add_vector_measurement('rudder angle', [state.delta_r* 180/np.pi], sim_time)
    plotter.add_vector_measurement('elevator angle', [state.delta_s* 180/np.pi], sim_time)

    # -------update viewer and video-------------
    mav_view.update(state, mav_path)

    # -------update the real time plotter --------#
    plotter.update_plots()
    
    if VIDEO is True:
        video.update(sim_time)

    time.append(sim_time)
    u.append(state.x.item(0))
    v.append(state.x.item(1))
    w.append(state.x.item(2))
    p.append(state.x.item(3)* 180/np.pi)
    q.append(state.x.item(4)* 180/np.pi)
    r.append(state.x.item(5)* 180/np.pi)
    x.append(state.x.item(6))
    y.append(state.x.item(7))
    z.append(state.x.item(8))
    phi.append(state.x.item(9)* 180/np.pi)
    theta.append(state.x.item(10)* 180/np.pi)
    psi.append(state.x.item(11)* 180/np.pi)
    RPM_Command.append(thruster_input)
    rudder_angle.append(rudder_input)
    elevator_angle.append(elevator_input)

##    print("Simulation Time: {:2.2f} | u: {:2.2f} | v: {:2.2f} | w: {:2.2f} | p: {:2.2f} | q: {:2.2f} | r: {:2.2f} | x: {:2.2f} | y: {:2.2f} | z: {:2.2f} | phi: {:2.2f} | theta: {:2.2f} | psi: {:2.2f}".format(sim_time,
##          state.x.item(0),
##          state.x.item(1),
##          state.x.item(2),
##          state.x.item(3)* 180/np.pi,
##          state.x.item(4)* 180/np.pi,
##          state.x.item(5)* 180/np.pi,
##          state.x.item(6),
##          state.x.item(7),
##          state.x.item(8),
##          state.x.item(9)* 180/np.pi,
##          state.x.item(10)* 180/np.pi,
##          state.x.item(11)* 180/np.pi),
##          end='\r')

    # -------increment time-------------
    sim_time += SIM.ts_simulation

    
print("Press Ctrl-Q to exit...")
if VIDEO is True:
    video.close()

fig, axs = plt.subplots(5, 3, sharex = True, sharey=False)
fig.suptitle('Vehicle State and Control Inputs')

axs[0, 0].plot(time, rudder_angle)
axs[0, 0].set(xlabel='Time (S)', ylabel='Rudder (D)')
axs[0, 0].grid(True, which='both', axis='both')

axs[0, 1].plot(time, elevator_angle)
axs[0, 1].set(xlabel='Time (S)', ylabel='Elevator (D)')
axs[0, 1].grid(True, which='both', axis='both')

axs[0, 2].plot(time, RPM_Command)
axs[0, 2].set(xlabel='Time (S)', ylabel='Thrust (RPMs)')
axs[0, 2].grid(True, which='both', axis='both')

axs[1, 0].plot(time, u)
axs[1, 0].set(xlabel='Time (S)', ylabel='u (m/s)')
axs[1, 0].grid(True, which='both', axis='both')

axs[1, 1].plot(time, v)
axs[1, 1].set(xlabel='Time (S)', ylabel='v (m/s)')
axs[1, 1].grid(True, which='both', axis='both')

axs[1, 2].plot(time, w)
axs[1, 2].set(xlabel='Time (S)', ylabel='w (m/s)')
axs[1, 2].grid(True, which='both', axis='both')

axs[2, 0].plot(time, p)
axs[2, 0].set(xlabel='Time (S)', ylabel='p (D/s)')
axs[2, 0].grid(True, which='both', axis='both')

axs[2, 1].plot(time, q)
axs[2, 1].set(xlabel='Time (S)', ylabel='q (D/s)')
#axs[2, 0].legend(['Input Current', 'Battery Input Current Limit'])
axs[2, 1].grid(True, which='both', axis='both')

axs[2, 2].plot(time, r)
axs[2, 2].set(xlabel='Time (S)', ylabel='r (D/s)')
#axs[2, 1].legend(['Output Current', 'Battery Output Current Limit'])
axs[2, 2].grid(True, which='both', axis='both')

axs[3, 0].plot(time, x)
axs[3, 0].set(xlabel='Time (S)', ylabel='X (m)')
axs[3, 0].grid(True, which='both', axis='both')

axs[3, 1].plot(time, y)
axs[3, 1].set(xlabel='Time (S)', ylabel='Y (m)')
axs[3, 1].grid(True, which='both', axis='both')

axs[3, 2].plot(time, z)
axs[3, 2].set(xlabel='Time (S)', ylabel='Z (m)')
axs[3, 2].grid(True, which='both', axis='both')

axs[4, 0].plot(time, phi)
axs[4, 0].set(xlabel='Time (S)', ylabel='Phi (D)')
axs[4, 0].grid(True, which='both', axis='both')

axs[4, 1].plot(time, theta)
axs[4, 1].set(xlabel='Time (S)', ylabel='Theta (D)')
#axs[2, 0].legend(['Input Current', 'Battery Input Current Limit'])
axs[4, 1].grid(True, which='both', axis='both')

axs[4, 2].plot(time, psi)
axs[4, 2].set(xlabel='Time (S)', ylabel='Psi (D)')
#axs[2, 1].legend(['Output Current', 'Battery Output Current Limit'])
axs[4, 2].grid(True, which='both', axis='both')

plt.tight_layout()
plt.show()



