"""
mavsim_python: mav viewer (for chapter 2)
    - Beard & McLain, PUP, 2012
    - Update history:
        2/24/2020 - RWB
"""
import sys
sys.path.append("..")
import numpy as np
import pyqtgraph as pg
import pyqtgraph.opengl as gl
import pyqtgraph.Vector as Vector
import matplotlib.pyplot as plt
from tools.drawing import drawMav, drawPath


class mavViewer():
    def __init__(self):
        # initialize Qt gui application and window
        self.app = pg.QtGui.QApplication([])  # initialize QT
        self.window = gl.GLViewWidget()  # initialize the view object
        self.camWindow = gl.GLViewWidget()  # initialize the camera view object
        self.window.setWindowTitle('MAV Viewer')
        self.camWindow.setWindowTitle('AUV Camera Vision')
        self.window.setGeometry(100, 100, 1000, 1000)  # args: upper_left_x, upper_right_y, width, height
        
        grid = gl.GLGridItem()  # make a grid to represent the ground
        grid.scale(1, 1, 1)  # set the size of the grid (distance between each line)
        grid.setSpacing(1, 1, 1)
        
        self.window.addItem(grid)  # add grid to viewer

        # Add Random Terrain
        y = np.linspace(-10000, 10000, 100)
        x = np.linspace(-10000,10000, 100)
        temp_z = np.random.rand(len(x),len(y))*100.0 -200
        cmap = plt.get_cmap('copper') # Try 'pink' 'copper' or 'terrain'
        minZ=np.min(temp_z)
        maxZ=np.max(temp_z)
        rgba_img = cmap((temp_z-minZ)/(maxZ -minZ))
        surf = gl.GLSurfacePlotItem(x=y, y=x, z=temp_z, colors = rgba_img )
        surf.scale(3, 1, 1)
        self.window.addItem(surf)
        self.camWindow.addItem(surf)
        
        
        self.window.setCameraPosition(distance=200) # distance from center of plot to camera
        self.window.setBackgroundColor(0, 100, 200)  # set background color to black
        self.window.show()  # display configured window

        self.camWindow.setCameraPosition(distance=10) 
        self.camWindow.setBackgroundColor(0, 100, 200)  
        self.camWindow.show()  

        self.window.raise_()  # bring window to the front
        self.plot_initialized = False  # has the mav been plotted yet?
        self.mav_plot = []
        self.cam_plot = []

        #New Added to draw path
        self.mav_path = []

    def update(self, state, path):
        # initialize the drawing the first time update() is called
        if not self.plot_initialized:
            self.mav_plot = drawMav(state, self.window)
            self.cam_plot = self.mav_plot
            
            self.mav_path = drawPath(path,color=[0.0,1.0,0.0,1.0], window=self.window)
            self.plot_initialized = True
        # else update drawing on all other calls to update()
        else:
            self.mav_plot.update(state)
            self.cam_plot = self.mav_plot
            self.mav_path.update(path)
        # update the center of the camera view to the mav location
        view_location = Vector(state.x.item(7), state.x.item(6), -state.x.item(8))  # defined in ENU coordinates
        camera_view_location = Vector(state.x.item(7) + state.L_auv, state.x.item(6), -state.x.item(8))
        self.window.opts['center'] = view_location
        self.camWindow.opts['center'] = camera_view_location
        print("Camera Position: ", self.camWindow.cameraPosition())
        # redraw
        self.app.processEvents()
