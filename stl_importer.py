#Extract all lines that start with "Vertex" in an ascii stl file

import numpy as np
        
def get_vertices(file_name, N):

    count = 0
    vertices = []
    mesh_line = []
    mesh = []
    
    with open(file_name, 'r') as read_obj:
        
        for line in read_obj:
            if count<N: # N allows you to limit the max number of vertices to load
                # For each line, check if line contains the string
                if 'vertex' in line:
                    count += 1
                    line = list(map(float, line.replace('vertex', "").strip().split(" ")))
                    vertices.append(line)

    return np.array(vertices).T            

def get_faces(points):
    result = points.T
    i, j = result.shape
    N = int(i/3)
    result = result.flatten()
    result = result.reshape(N, 3, 3)
    return result
