### Rotation.py
import numpy as np
import math as m

def Euler2Rotation(phi, theta, psi):
    R_roll = np.array([[1, 0, 0],
                       [0, m.cos(phi), m.sin(phi)],
                       [0, -m.sin(phi), m.cos(phi)]])
    
    R_pitch = np.array([[m.cos(theta), 0, -m.sin(theta)],
                       [0, 1, 0],
                       [m.sin(theta), 0, m.cos(theta)]])
    
    R_yaw = np.array([[m.cos(psi), m.sin(psi), 0],
                      [-m.sin(psi), m.cos(psi), 0],
                      [0, 0, 1]])

    R = R_roll @ R_pitch @ R_yaw
    return R.T

