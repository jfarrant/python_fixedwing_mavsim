# STL Model Loader for Small Unmanned Aircraft: Theory and Practice
![SimExample](images/Sim_Example.JPG)

Credit: Most of the code contained within this repository was taken from the 
following website:
 
https://uavbook.byu.edu/doku.php

# Purpose
This repository contains instructions on how to use a CAD model with the Python MAV Simulator from the uavbook course website. A custom STL_Importer.py 
file is added to parse through an ascii STL file and load the vertices into numpy arrays for use with the MavSimulator software. This repository 
contains a custom UAV STL file "Spaerohawk.stl" which is included for demonstration purposes. To use your own CAD model, simply follow the instructions
below, and replace the "Spaerohawk.stl" with your own file.
![Spaerohawk](images/Spaerohawk_001.JPG)

# Instructions for running simulation with provided Spaerohawk CAD model
1. Ensure you have all of the Python dependencies from the uavbook course website installed first.
2. Clone this repo
3. Using Idle or your favorite Python editor, navigate to the chapter 2 folder.
4. Open and run mavsim_chap2.py

# Instructions for using your own CAD model
1. Create the model in Solidworks
2. Ensure that the Center of Mass is coincident with the origin. I acomplish this 
using the following steps:  
a. With the model loaded into a Solidworks Assembly, open the mass properties window, and take note of the X,Y,Z COG offset  
![Mass_Properties](images/Mass_Properties.JPG)

b. close the mass properties window, right click on the part and select float so that it can be moved.(f) means that it is fixed, 
while (-) means that it is floating.
![float](images/float.JPG)
![floating](images/floating.JPG)

c. Move the model so that the COG is aligned with the origin by selecting tools->component->move  

d. In the move options box, change the drop down menu to: "bydeltaXYZ" and enter the negative of the values noted in step a.  
e. Your models COG should now be aligned with the coordinate systems origin.
![COG_Updated](images/COG_Updated.JPG)

3. Save the model as an STL file, and make sure to press the options button before saving and check the ascii box, 
and make sure that you save using the same units the model is created in.
![STL_Settings](images/STL_Settings.JPG)
![IPS](images/IPS.JPG)

# Use Blender to minimize the number of triangles (Shrink the STL file size)
Blender is a powerful, free 3D Modeling software. We use it in the following instructions
to shrink the size of the STL file so that it does not slow the performance of the Mav Simulator.

Blender Decimate-  
1. Open Blender  
2. Delete the cube, (press delete when cube is selected)
![Delete_Cube](images/Delete_Cube.JPG)  
3. File->Import->STL, navigate to your CAD file and import it.  
4. Use the Transform tool on the right to rotate the model to reflect the normal orientation (+X forward, +Z up). This will be corrected 
so that it is compatable with the MavSimulator during export options.  
![Rotations](images/Rotations.JPG)   
5. Change into Edit mode.
![Edit_Mode](images/Edit_Mode.JPG)   
6. Select Mesh->Cleanup->Decimate Geometry, change the ratio to appx 0.26. I have found this works well without distorting the original model too much. Feel
free to change this to whatever works best for you.
![Decimated](images/Decimated.JPG)  
7. Select File->Export->STL, make sure to check the ASCII box, and -Z, -Y.
![Export](images/Export.JPG)   
8. Notice the size of the Decimated STL file is much smaller than the original  
9. You are now ready to use your model. Replace the "Spaerohawk.stl" model provided in this repo with your custom model.