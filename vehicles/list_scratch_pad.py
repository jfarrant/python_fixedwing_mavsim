import numpy as np


def Smtrx(a):
    a = a.flatten()
    S = np.array([[0, -a[2], a[1]],
                  [a[2], 0, -a[0]],
                  [-a[1], a[0], 0]])
    print("Smtrx: \n", S, "\n")
    return S

def Hmtrx(r):
    S = Smtrx(r)
    #print("S: ", S)
    H1 = np.concatenate((np.eye(3), S.T), axis=1)
    #print("H1: ", H1)
    H2 = np.concatenate((np.zeros((3, 3)), np.eye(3)), axis=1)
    #print("H2: ", H2)
    H = np.concatenate((H1, H2), axis=0)
    #H = np.array([np.eye(3), S.T], [np.zeros((3, 3)), np.eye(3)])
    #print("H: ", H)
    return H

def spheroid(a, b, nu2, r_bg, rho=1026.0):
    O3 = np.zeros((3, 3))
    print("O3: \n", O3, "\n")
    print("O3 type: \n", type(O3), "\n")

    m = 4/3.0 * np.pi * rho * a * b**2
    print("m: \n", m, "\n")
    print("m type: \n", type(m), "\n")

    #moment of inertia
    Ix = (2/5.0) * m * b**2
    Iy = (1/5.0) * m * (a**2 + b**2)
    Iz = Iy
    Ig = np.array([[Ix, 0.0, 0.0], [0.0, Iy, 0.0], [0.0, 0.0, Iz]])
    print("Ig: \n", Ig, "\n")
    print("IG type: \n", type(Ig), "\n")
    #rigid-body matrices expressed in the CG
    MRB_CG = np.array([[m, 0, 0, 0,  0, 0 ],
                       [0, m, 0, 0,  0, 0 ],
                       [0, 0, m, 0,  0, 0 ],
                       [0, 0, 0, Ix, 0, 0 ],
                       [0, 0, 0, 0,  Iy, 0],
                       [0, 0, 0, 0,  0, Iz]])

    print("MRB_CG: \n", MRB_CG, "\n")
    print("MRB_CG type: \n", type(MRB_CG), "\n")

    CRB_CG_00 = m * Smtrx(nu2)
    xxx = np.concatenate((CRB_CG_00, O3), axis=1)
    yyy = np.concatenate((O3, -Smtrx(Ig@nu2)), axis=1)
    print("xxx: \n", xxx, "\n")
    print("yyy: \n", yyy, "\n")
    print("CRB_CG_00: \n", CRB_CG_00, "\n")

    CRB_CG = np.concatenate((xxx, yyy), axis=0)
    print("CRB_CG: \n", CRB_CG, "\n")
    print("CRB_CG type: \n", type(CRB_CG), "\n")
    
    print("r_bg:\n", r_bg)
    print(type(r_bg))
    H = Hmtrx(r_bg)

    #print("H: ", H)
    #print(H.T)
    MRB = H.T @ MRB_CG @ H
    CRB = H.T @ CRB_CG @ H

    return MRB, CRB

u = 0
v = 0
w = 0
p = 0
q = 0
r = 0

r_bg = np.array([[0, 0, 0.02]]).T
L_auv = 1.7
a = L_auv/2.0
D_auv = 0.19
b = D_auv/2.0
nu = np.array([[u, v, w, p, q, r]]).T
print(nu)

print("nu: \n", nu[3:6], "\n")

MRB,CRB = spheroid(a,b,nu[3:6],r_bg)



print("MRB: \n", MRB, "\n")

print("CRB: \n", CRB, "\n")
