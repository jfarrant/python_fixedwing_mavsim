# ADC_MK5
"""
 state vector: x = [ u v w p q r x y z phi theta psi ]' and speed U in m/s  
 (optionally) for the Remus 100 autonomous underwater vehicle (AUV). The 
 length of the AUV is L = 1.7 m, while the state vector is defined as:

  u:       surge velocity          (m/s)
  v:       sway velocity           (m/s)
  w:       heave velocity          (m/s)
  p:       roll rate               (rad/s)
  q:       pitch rate              (rad/s)
  r:       yaw rate                (rad/s)
  x:       North position          (m)
  y:       East position           (m)
  z:       downwards position      (m)
  phi:     roll angle              (rad)
  theta:   pitch angle             (rad)
  psi:     yaw angle               (rad)

 The control inputs are:

  ui = [ delta_r delta_s n ]'  where

    delta_r:   rudder angle (rad)
    delta_s:   aft stern plane (rad) 
    n:         propeller revolution (rpm)

 The last arguments Vc and betaVc are optional arguments for ocean current 
 speed and direction expressed in NED.
"""
import numpy as np

def ADC_MK5(x, ui, Vc=0.0, betaVc=0.0):
    if(len(x) != 12):
        raise Exception("x-vector must have dimension 12!")
    if(len(ui) != 3):
        raise Exception("u-vector must have dimension 3!")

    """ Constants """
    #Definitely Static
    mu = 47.8209    #Lattitude for Lynnwood Washington
    #Definitely Static
    g_mu = gravity(mu) #Gravity vector (m/s2)
    #Definitely Static
    rho = 1026 #Density of water (m/s2)

    """ State vectors and control inputs """
    
    nu = np.array(x[0:5])
    eta = np.array(x[6:11])
    delta_r = np.array(ui[0]) # Tail Rudder (Rad)
    delta_s = np.array(ui[1]) # Stern Plane (Rad)
    n = np.array(ui[2]/60)    # propeller revolution (rps)

    """ Ocean currents expressed in Body """
    u_c = Vc * np.cos(betaVc - eta[5])
    v_c = Vc * np.sin(betaVc - eta[5])

    """ Amplitude Saturation of control signals """
    max_ui = np.array([30*np.pi/180, 50*np.pi/180, 1500/60]) # deg, deg, rps

    """ Relative velocities, speed and angle of attack """
    nu_r = nu - [u_c v_c 0.0 0.0 0.0 0.0]                   # relative velocity
    alpha = np.arctan2( nu_r[2], nu_r[0] )                # angle of attack (rad)
    U_r = np.sqrt( nu_r[0]**2 + nu_r[1]**2 + nu_r[2]**2 )  # relative speed (m/s)
    U  = np.sqrt( nu[0]**2 + nu[1]**2 + nu[2]**2 )         # speed (m/s)

    """ AUV model parameters """
    L_auv = 1.7                        # AUV length (m)
    D_auv = 0.19                       # AUV diamater (m)
    CD_0 = 0.2                         # parasitic drag
    S = 0.7 * L_auv * D_auv            # S = 70% of rectangle L_auv * D_auv
    a = L_auv/2                        # semi-axes
    b = D_auv/2                  
    r_bg = [ [0], [0], [0.02] ]               # CG w.r.t. to the CO
    r_bb = [ [0], [0], [0] ]                  # CB w.r.t. to the CO

    """ Added moment of inertia in roll """
    r44 = 0.3               # A44 = r44 * Ix

    """ Propeller data """
    D_prop = 0.055          # propeller diameter
    KT = 0.4739             # [KT, KQ] = wageningen(0,1,0.8,3)
    KQ = 0.0730

    """ Tail rudder """
    CL_delta_r = 0.5        # rudder lift coefficient
    A_r = 0.10 * 0.05       # rudder area (m2)
    x_r = -a                # rudder x-position (m)

    """ Stern plane """
    CL_delta_s = 0.7        # stern-plane lift coefficient
    A_s = 2 * 0.10 * 0.05   # stern-plane area (m2)
    x_s = -a                # stern-plane z-position (m)

    """ Low-speed linear damping matrix parameters: D * exp(-3 * U_r) """
    T1 = 20                 # time constant in surge (s)
    T2 = 20                 # time constant in sway (s)
    zeta4 = 0.3             # relative damping ratio in roll
    zeta5 = 0.8             # relative damping ratio in pitch
    T6 = 5                  # time constant in yaw (s)

    """ mass and added mass """
    MRB,CRB = spheroid(a,b,nu[3:5],r_bg)
    MA,CA = imlay61(a, b, nu_r, r44)

    """ nonlinear quadratic velocity terms in pitch and yaw (Munk moments) 
        are cancelled since only linear damping is used
    """
    CA[4][0] = 0   
    CA[4][3] = 0
    CA[5][0] = 0
    CA[5][1] = 0
    M = MRB + MA
    C = CRB + CA
    
    m = MRB[0][0]
    W = m*g_mu
    B = W
    
    """ dissipative forces and moments """
    
    D = Dmtrx([T1 T2 T6],[zeta4 zeta5],MRB,MA,[W r_bg.T r_bb.T])
    D[0][0] = D[0][0] * exp(-3*U_r)   # vanish at high speed where crossflow
    D[1][1] = D[1][1] * exp(-3*U_r)   # drag and lift/drag dominates
    D[5][5] = D[5][5] * exp(-3*U_r)
    
    tau_liftdrag = forceLiftDrag(D_auv,S,CD_0,alpha,U_r)
    tau_crossflow = crossFlowDrag(L_auv,D_auv,D_auv,nu_r)

    """ restoring forces and moments """
    g = gvect(W,B,eta[4],eta[3],r_bg,r_bb)

    """ kinematics """
    J = eulerang(eta[3],eta[4],eta[5])

    """ amplitude saturation of the control signals """
    if (abs(delta_r) > max_ui[0]):
        delta_r = sign(delta_r) * max_ui[0]
        
    if (abs(delta_s) > max_ui[1]):
        delta_s = sign(delta_s) * max_ui[1]
        
    if (abs(n) > max_ui[2]):
        n = sign(n) * max_ui[2]
        
    """ control forces and moments """
    X_prop = rho * D_prop**4 * KT * abs(n) * n  # propeller thrust 
    K_prop = rho * D_prop**5 * KQ * abs(n) * n  # propeller-induced roll moment
    X_r = -0.5 * rho * U_r**2 * A_r * CL_delta_r * delta_r**2  # rudder drag
    Y_r = -0.5 * rho * U_r**2 * A_r * CL_delta_r * delta_r    # rudder sway force
    N_r = x_r * Y_r                                          # rudder yaw moment
    X_s = -0.5 * rho * U_r**2 * A_s * CL_delta_s * delta_s**2  # stern-plane drag
    Z_s = -0.5 * rho * U_r**2 * A_s * CL_delta_s * delta_s    # stern-plane heave force
    M_s =  x_s * Z_s                                         # stern-plane pitch moment
    tau = np.zeros(6)                                # generalized force vector
    tau[0] = X_prop + X_r + X_s
    tau[1] = Y_r
    tau[2] = Z_s
    tau[3] = K_prop
    tau[4] = M_s
    tau[5] = N_r

    """ state-space model """
    return np.linalg.inv(M) @ (tau + tau_liftdrag + tau_crossflow - C * nu_r - D * nu_r  - g), J * nu
